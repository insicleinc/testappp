package com.example.sword.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sword.R;
import com.example.sword.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentHolder> {

    List<Student> students = new ArrayList<>();
    InterfaceViewClickListener recyclerViewClickListener;

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_item, parent, false);
        return new StudentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.StudentHolder holder, int position) {
        Student student = students.get(position);
        holder.textViewRollNo.setText("Roll No. : " + String.valueOf(student.getRollNo()));
        holder.textViewName.setText("Name : " + student.getName());
        holder.textViewFatherName.setText("Father's Name : " + student.getFatherName());
        holder.textViewAge.setText("Student's Age : " + String.valueOf(student.getAge()));



    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public void initAdapter(List<Student> students) {
        this.students = students;
        notifyDataSetChanged();
    }

    public void setOnClickListener(InterfaceViewClickListener interfaceViewClickListener) {
        this.recyclerViewClickListener = interfaceViewClickListener;
    }

    class StudentHolder extends RecyclerView.ViewHolder {

        private TextView textViewRollNo;
        private TextView textViewName;
        private TextView textViewFatherName;
        private TextView textViewAge;
        private Button deleteButton;
        private Button editButton;

        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            textViewRollNo = itemView.findViewById(R.id.text_view_roll_no);
            textViewName = itemView.findViewById(R.id.text_view_name);
            textViewFatherName = itemView.findViewById(R.id.text_view_father_name);
            textViewAge = itemView.findViewById(R.id.text_view_age);
            deleteButton = itemView.findViewById(R.id.button_delete);
            editButton = itemView.findViewById(R.id.button_edit);

            deleteButton.setOnClickListener(view -> {
                int position = getAdapterPosition();
                recyclerViewClickListener.deleteStudent(students.get(position));
            });

            editButton.setOnClickListener(view ->{
                int position = getAdapterPosition();
                recyclerViewClickListener.updateStudent(students.get(position));
            });

        }
    }


    public interface InterfaceViewClickListener {
        void deleteStudent(Student student);
        void updateStudent(Student student);

    }

}

