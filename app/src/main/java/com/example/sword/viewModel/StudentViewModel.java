package com.example.sword.viewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.sword.model.Student;
import com.example.sword.repository.StudentRepository;

import java.util.List;

public class StudentViewModel extends AndroidViewModel {

    private StudentRepository studentRepository;
    private LiveData<List<Student>> allStudents;

    public StudentViewModel(@NonNull Application application) {
        super(application);
        studentRepository = new StudentRepository(application);
        allStudents = studentRepository.getAllStudents();
    }

    public void insert(Student student){
        studentRepository.insertStudent(student);
    }
    public void update(Student student){
        studentRepository.updateStudent(student);
    }
    public void delete(Student student){
        studentRepository.deleteStudent(student);
    }
    public void deleteAllStudents(Student student){
        studentRepository.deleteAllStudent(student);
    }
    public LiveData<List<Student>> getAllStudents(){
        return allStudents;
    }

}
