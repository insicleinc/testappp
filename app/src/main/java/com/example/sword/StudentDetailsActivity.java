package com.example.sword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

public class StudentDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_ROLL_NO = "com.example.sword.STUDENT_ROLL_NO";
    public static final String EXTRA_NAME = "com.example.sword.STUDENT_NAME";
    public static final String EXTRA_FATHER_NAME = "com.example.sword.STUDENT_FATHER_NAME";
    public static final String EXTRA_AGE = "com.example.sword.STUDENT_AGE";

    private Button buttonSubmit;
    private EditText editTextStudentName;
    private EditText editTextFatherName;
    private NumberPicker numberPickerAge;
    private TextView textViewTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);

        editTextStudentName = findViewById(R.id.edit_text_student_name);
        editTextFatherName = findViewById(R.id.edit_text_student_father_name);
        textViewTitle = findViewById(R.id.text_title);
        numberPickerAge = findViewById(R.id.edit_text_age);

        numberPickerAge.setMinValue(4);
        numberPickerAge.setMaxValue(35);

        buttonSubmit = findViewById(R.id.button_submit);

        buttonSubmit.setOnClickListener((View v) -> {
            saveStudentData();
        });

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ROLL_NO)) {
            textViewTitle.setText("Edit Student Details");
            editTextStudentName.setText(intent.getStringExtra(EXTRA_NAME));
            editTextFatherName.setText(intent.getStringExtra(EXTRA_FATHER_NAME));
            numberPickerAge.setValue(intent.getIntExtra(EXTRA_AGE, 0));


        } else {
            textViewTitle.setText("Add Student Details");

        }


    }

    //ToDo Validation
    private void saveStudentData() {
        String studentName = editTextStudentName.getText().toString().trim();
        String studentFatherName = editTextFatherName.getText().toString().trim();
        int studentAge = numberPickerAge.getValue();

        if (validateData(studentName, studentFatherName, studentAge)) {

            Intent data = new Intent();
            data.putExtra(EXTRA_NAME, studentName);
            data.putExtra(EXTRA_FATHER_NAME, studentFatherName);
            data.putExtra(EXTRA_AGE, studentAge);

            int rollNo = getIntent().getIntExtra(EXTRA_ROLL_NO, -1);

            if (rollNo != -1) {
                data.putExtra(EXTRA_ROLL_NO, rollNo);
            }

            setResult(RESULT_OK, data);
            finish();
        }

    }

    private boolean validateData(String studentName, String studentFatherName, int studentAge) {

        if (studentName.length() == 0) {
            editTextStudentName.setError("This field is required");
            return false;
        }

        if (!isFullname(studentName)) {
            editTextStudentName.setError("Student Name can contain only alphabets or spaces.");
            return false;
        }

        if (studentFatherName.length() == 0) {
            editTextFatherName.setError("This field is required");
            return false;
        }

        if (!isFullname(studentFatherName)) {
            editTextFatherName.setError("Father's name can contain only alphabets or spaces.");
            return false;
        }

        return true;
    }

    public static boolean isFullname(String str) {

        for (int i = 0; i < str.length() - 1; i++) {
            char c = str.charAt(i);
            char c1 = str.charAt(i + 1);
            if (!ifChar(c) && !ifChar(c1)) {
                return false;
            }
        }

        if (!ifChar(str.charAt(str.length() - 1))) {
            return false;
        }
        return true;
    }

    private static boolean ifChar(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
}