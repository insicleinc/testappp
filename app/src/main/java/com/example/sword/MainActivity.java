package com.example.sword;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.sword.adapter.StudentAdapter;
import com.example.sword.model.Student;
import com.example.sword.viewModel.StudentViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int GET_DETAILS_REQUEST = 9987889;
    public static final int EDIT_DETAILS_REQUEST = 9685485;
    public static final int CREATE_DETAILS_REQUEST = 152478;

    private StudentViewModel studentViewModel;

    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;
    private Button editButton;
    private Button deleteButton;

    StudentAdapter.InterfaceViewClickListener recyclerViewClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyc_view);
        floatingActionButton = findViewById(R.id.add_student);

        floatingActionButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(MainActivity.this, StudentDetailsActivity.class);
            createStudentLauncher.launch(intent);
        });

        recyclerView = findViewById(R.id.recyc_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        StudentAdapter studentAdapter = new StudentAdapter();
        recyclerView.setAdapter(studentAdapter);
        studentAdapter.setOnClickListener(new StudentAdapter.InterfaceViewClickListener() {
            @Override
            public void deleteStudent(Student student) {
                studentViewModel.delete(student);
            }

            @Override
            public void updateStudent(Student student) {

                Intent intent = new Intent(MainActivity.this, StudentDetailsActivity.class);
                intent.putExtra(StudentDetailsActivity.EXTRA_NAME, student.getName());
                intent.putExtra(StudentDetailsActivity.EXTRA_FATHER_NAME, student.getFatherName());
                intent.putExtra(StudentDetailsActivity.EXTRA_AGE, student.getAge());
                intent.putExtra(StudentDetailsActivity.EXTRA_ROLL_NO, student.getRollNo());

                editStudentLauncher.launch(intent);

            }
        });


        studentViewModel = new ViewModelProvider(this).get(StudentViewModel.class);
        studentViewModel.getAllStudents().observe(this, new Observer<List<Student>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onChanged(List<Student> students) {
                studentAdapter.initAdapter(students);
            }
        });


    }

    ActivityResultLauncher<Intent> createStudentLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();

                        assert data != null;
                        String studentName = data.getStringExtra(StudentDetailsActivity.EXTRA_NAME);
                        String studentFatherName = data.getStringExtra(StudentDetailsActivity.EXTRA_FATHER_NAME);
                        int studentAge = data.getIntExtra(StudentDetailsActivity.EXTRA_AGE, 1);



                            Student student = new Student(studentName, studentFatherName, studentAge);
                            studentViewModel.insert(student);

                            Toast.makeText(MainActivity.this, "Details added successfully.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MainActivity.this, "Student details not saved.", Toast.LENGTH_SHORT).show();

                    }
                }
            });



    ActivityResultLauncher<Intent> editStudentLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();

                        int rollNo = data.getIntExtra(StudentDetailsActivity.EXTRA_ROLL_NO, -1);

                        if (rollNo == -1) {
                            Toast.makeText(MainActivity.this, "Student details not changed.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        String studentName = data.getStringExtra(StudentDetailsActivity.EXTRA_NAME);
                        String studentFatherName = data.getStringExtra(StudentDetailsActivity.EXTRA_FATHER_NAME);
                        int studentAge = data.getIntExtra(StudentDetailsActivity.EXTRA_AGE, 1);

                        Student student = new Student(studentName, studentFatherName, studentAge);
                        student.setRollNo(rollNo);

                        studentViewModel.update(student);

                        Toast.makeText(MainActivity.this, "Details updated successfully.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MainActivity.this, "Student details not changed.", Toast.LENGTH_SHORT).show();

                    }
                }
            });


}