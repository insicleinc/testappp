package com.example.sword.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.sword.db.StudentDatabase;
import com.example.sword.dao.StudentDao;
import com.example.sword.model.Student;

import java.util.List;

public class StudentRepository {
    private StudentDao studentDao;
    private LiveData<List<Student>> allStudents;

    public StudentRepository(Application application){
        super();
        StudentDatabase database = StudentDatabase.getInstance(application);
        studentDao = database.studentDao();
        allStudents = studentDao.fetchAllStudents();
    }

    public void insertStudent(Student student){
        new insertStudentAsyncTask(studentDao).execute(student);
    }
    public void updateStudent(Student student){
        new updateStudentAsyncTask(studentDao).execute(student);

    }
    public void deleteStudent(Student student){
        new deleteStudentAsyncTask(studentDao).execute(student);
    }
    public void deleteAllStudent(Student student){
        new deleteAllStudentAsyncTask(studentDao).execute();
    }
    public LiveData<List<Student>> getAllStudents(){
        return allStudents;
    }

    private static class insertStudentAsyncTask extends AsyncTask<Student, Void, Void>{

        private StudentDao studentDao;

        public insertStudentAsyncTask(StudentDao studentDao) {
            this.studentDao = studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            studentDao.insert(students[0]);
            return null;
        }
    }

     private static class updateStudentAsyncTask extends AsyncTask<Student, Void, Void>{

        private StudentDao studentDao;

        public updateStudentAsyncTask(StudentDao studentDao) {
            this.studentDao = studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            studentDao.update(students[0]);
            return null;
        }
    }

     private static class deleteStudentAsyncTask extends AsyncTask<Student, Void, Void>{

        private StudentDao studentDao;

        public deleteStudentAsyncTask(StudentDao studentDao) {
            this.studentDao = studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            studentDao.delete(students[0]);
            return null;
        }
    }


 private static class deleteAllStudentAsyncTask extends AsyncTask<Void, Void, Void>{

        private StudentDao studentDao;

        public deleteAllStudentAsyncTask(StudentDao studentDao) {
            this.studentDao = studentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            studentDao.deleteAll();
            return null;
        }
    }



}
